
var responsiveDesign = function()
{
    let documentWidth = $(window).width();

    if (documentWidth < 1050) {

        let headerImagesSize;

        if (documentWidth > 920) {
            headerImagesSize = (documentWidth - 450) / 5;
        } else {
            headerImagesSize = (documentWidth - 450) / 4;
        }

        $headerImages = $('.header-images-inner-2 img').each(function () {
            $(this).width(headerImagesSize);
            $(this).height(headerImagesSize);
        });

        $('.header-images-inner-1').height(headerImagesSize + 40);


        if (documentWidth > 930) {
            $('.header-female').css('top', (-64 + headerImagesSize));
        }

        if (documentWidth < 930 && documentWidth > 800) {
            $('.header-female').css('top', (-46 + headerImagesSize));
        }
    }


    if (documentWidth < 800 && documentWidth > 450) {
        $('.homepage-massage img').each(function () {
            let topOffset = $(this).parent().height() / 2 - 100;
            $(this).css('top', topOffset + 'px');
        });
    }


    if (documentWidth < 930) {
        let menuPaddingLeft = (documentWidth - 750) / 2;
        $('.header-menu-top-inner').css('padding-left', menuPaddingLeft + 'px');
        $('.header-menu-bottom-inner-2').css('padding-left', (menuPaddingLeft + 3) + 'px');
    } else {
        $('.header-menu-top-inner').css('padding-left', '100px');
        $('.header-menu-bottom-inner-2').css('padding-left', '191px');
    }


    let menuItems = {
        'menu-index':           $('.menu-index'),
        'menu-team':            $('.menu-team'),
        'menu-services':        $('.menu-services'),
        'menu-school':          $('.menu-school'),
        'menu-photogallery':    $('.menu-photogallery'),
        'menu-articles':        $('.menu-articles'),
        'menu-contact':         $('.menu-contact'),
        'menu-about':           $('.menu-about'),
        'menu-eshop':           $('.menu-eshop'),
        'menu-luxury-massages': $('.menu-luxury-massages'),
        'menu-baths':           $('.menu-baths'),
        'menu-more':            $('.menu-more')
    };

    let $topMenu =        $('.header-menu-top-inner');
    let $bottomMenu =     $('.header-menu-bottom-inner-2');
    let $bottomMenuMore = $('.menu-more ul');

    let menu = {
        '1': {
            'top': ['menu-index', 'menu-team', 'menu-services', 'menu-school', 'menu-photogallery', 'menu-articles', 'menu-contact'],
            'bottom': ['menu-about', 'menu-eshop', 'menu-luxury-massages', 'menu-baths'],
            'more': []
        },
        '2': {
            'top': ['menu-index', 'menu-team', 'menu-services', 'menu-school', 'menu-photogallery', 'menu-articles'],
            'bottom': ['menu-contact', 'menu-about', 'menu-eshop', 'menu-luxury-massages', 'menu-baths'],
            'more': []
        },
        '3': {
            'top': ['menu-index', 'menu-team', 'menu-services', 'menu-school', 'menu-photogallery'],
            'bottom': ['menu-articles', 'menu-contact', 'menu-about', 'menu-eshop'],
            'more': ['menu-luxury-massages', 'menu-baths']
        },
        '4': {
            'top': ['menu-index', 'menu-team', 'menu-services', 'menu-school'],
            'bottom': ['menu-photogallery', 'menu-contact', 'menu-eshop'],
            'more': ['menu-articles', 'menu-about', 'menu-luxury-massages', 'menu-baths']
        },
        '5': {
            'top': ['menu-index', 'menu-team', 'menu-services'],
            'bottom': ['menu-school', 'menu-contact'],
            'more': ['menu-photogallery', 'menu-articles', 'menu-about', 'menu-eshop', 'menu-luxury-massages', 'menu-baths']
        },
    };

    var buildMenu = function (menuIndex) {
        $.each(menu[menuIndex]['top'], function () {
            $topMenu.append(menuItems[this]);
        });
        $.each(menu[menuIndex]['bottom'], function () {
            $bottomMenu.append(menuItems[this]);

            if (menu[menuIndex]['more'].length >= 1) {
                $bottomMenu.append(menuItems['menu-more']);
            }
        });
        $.each(menu[menuIndex]['more'], function () {
            $bottomMenuMore.append(menuItems[this]);
        })
    };

    if (documentWidth >= 700) {
        buildMenu(1);
    }
    if (documentWidth < 700 && documentWidth >= 650) {
        buildMenu(2);
    }
    if (documentWidth < 650 && documentWidth >= 550) {
        buildMenu(3);
    }
    if (documentWidth < 550 && documentWidth >= 440) {
        buildMenu(4);
    }
    if (documentWidth < 440) {
        buildMenu(5);
    }

    console.log('resize to ' + documentWidth);
};


$(window).on('resize', function() {
    responsiveDesign();
});

$(document).ready(function () {
    responsiveDesign();
});


